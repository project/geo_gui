var maps = [];  
var current_map, map_proj, wkt,geo_to_ol_types;

$(document).ready(function() {
  
   //Some global options
   OpenLayers.Feature.Vector.style['default']['strokeWidth'] = '2';
   OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
   OpenLayers.Util.onImageLoadErrorColor = "transparent";
  
   //Global objects
   map_proj = new OpenLayers.Projection("EPSG:900913");
    wkt  = new OpenLayers.Format.WKT();
    geo_to_ol_types = {
      polygon:  "Polygon",
      linestring: "LineString",
      point: "Point"
    }
  
   //Some global triggers
   document.onkeydown = OnKeyDown;
   $("#node-form").submit(function(){
     for(var map_name in maps) {
      if (maps[map_name]['active']) map_to_wkt(map_name);
    }
   });
  
  for (var i in geo_gui_params){
    var params = geo_gui_params[i];
    var map_name = params['name'];
    
    //Adding our HTML for the map and control buttons
    $("#"+params['name']+"-items")
      .css("display","none")
      .after("<div class='form-item' id='map_container_"+map_name+"' onmouseover=\"current_map = '"+map_name+"';\"></div>");
    $("#map_container_"+map_name)
      .append("<label>"+params['label']+":</label>")
      .append("<div id='geo_gui_map_"+map_name+"' style='height:400px; width:100%'></div>")
       .append("<a href='#'><img id='pan_"+map_name+"'    alt='Pan and zoom the map'  src='"+geo_gui_module_path+"/pan_on.png'                         onclick =\"pan_switch('"+map_name+"'); return false;\"    style='float:right; z-index:1000; position:relative; bottom:400px; margin:2px;' /></a>")
       .append("<a href='#'><img id='draw_"+map_name+"'   alt='Draw and edit shapes'  src='"+geo_gui_module_path+"/draw_"+params['type']+"_off.png'   onclick =\"draw_switch('"+map_name+"'); return false;\"   style='float:right; z-index:1000; position:relative; bottom:400px; margin:2px;' /></a>")
       .append("<div class='description'>"+params['description']+"</div>")
       .append("<div class='description'>Click the tools in the upper right-hand corner of the map to switch between draw mode and zoom/pan mode. Draw your shape, double-clicking to finish. You may edit your shape using the control points. To delete a shape, select it and press the delete key.</div>")
      .after("<p><a id='switch_map_wkt_link_"+map_name+"' href='#' onclick =\"switch_map_wkt('"+map_name+"'); return false;\">Edit using WKT</a></p>");   
    
     //Setting up our maps
     maps[map_name] = [];
     maps[map_name]['active'] = true;
    maps[map_name]['type'] = params['type'];
     maps[map_name]['dbproj'] = new OpenLayers.Projection("EPSG:"+params['dbproj']);
    
    var options = {
                projection: map_proj,
                displayProjection: maps[map_name]['dbproj'],
                units: "m",
                numZoomLevels: 18,
                maxResolution: 156543.0339,
                maxExtent: new OpenLayers.Bounds(-20037508, -20037508,
                                                 20037508, 20037508.34)
     };
     
     maps[map_name]['map'] = new OpenLayers.Map("geo_gui_map_"+map_name, options);
    maps[map_name]['map'].map_name = map_name;
    
    //Add Layers
    maps[map_name]['layers'] = [];
    
    // create Google Mercator layers
    maps[map_name]['layers']['gmap'] = new OpenLayers.Layer.Google(
      "Google Streets",
      {'sphericalMercator': true}
    );
    
    maps[map_name]['layers']['gsat'] = new OpenLayers.Layer.Google(
      "Google Satellite",
      {type: G_SATELLITE_MAP, 'sphericalMercator': true, numZoomLevels: 22}
     );
    
    maps[map_name]['layers']['ghyb'] = new OpenLayers.Layer.Google(
        "Google Hybrid",
        {type: G_HYBRID_MAP, 'sphericalMercator': true}
    );

    maps[map_name]['layers']['gphy'] = new OpenLayers.Layer.Google(
      "Google Physical",
      {type: G_PHYSICAL_MAP, 'sphericalMercator': true}
    );
    
     var myStyles = new OpenLayers.StyleMap({
                "default": new OpenLayers.Style({
                    pointRadius: 5, // sized according to type attribute
                    fillColor: "#ffcc66",
                    strokeColor: "#ff9933",
                    strokeWidth: 4,
                    fillOpacity:0.5
                }),
                "select": new OpenLayers.Style({
                    fillColor: "#66ccff",
                    strokeColor: "#3399ff"
                })
    });
    
    maps[map_name]['layers']['vectors'] = new OpenLayers.Layer.Vector(params['label'],{styleMap: myStyles});

    maps[map_name]['map'].addLayers([maps[map_name]['layers']['gphy'], maps[map_name]['layers']['gsat'], maps[map_name]['layers']['gmap'], maps[map_name]['layers']['vectors']]);
    
    //Add Controls
    maps[map_name]['map'].addControl(new OpenLayers.Control.LayerSwitcher());
    
    if (maps[map_name]['type'] == 'point')       var create_control = new OpenLayers.Control.DrawFeature(maps[map_name]['layers']['vectors'], OpenLayers.Handler.Point);
    if (maps[map_name]['type'] == 'linestring') var create_control = new OpenLayers.Control.DrawFeature(maps[map_name]['layers']['vectors'], OpenLayers.Handler.Path);
    if (maps[map_name]['type'] == 'polygon')     var create_control = new OpenLayers.Control.DrawFeature(maps[map_name]['layers']['vectors'], OpenLayers.Handler.Polygon);
    
    maps[map_name]['edit_controls'] = {
      create: create_control,
      modify: new OpenLayers.Control.ModifyFeature(maps[map_name]['layers']['vectors'])
    };
            
    for(var key in maps[map_name]['edit_controls']) {
      maps[map_name]['map'].addControl(maps[map_name]['edit_controls'][key]);
    }
        
    maps[map_name]['edit_controls'].create.events.register('featureadded',maps[map_name]['edit_controls'].create,feature_added);
    
    maps[map_name]['centerlatlon'] = new OpenLayers.LonLat(0,0)
    maps[map_name]['map'].setCenter(maps[map_name]['centerlatlon'], 2);
    
    //Pull the values from WKT fields and populate map
    wkt_to_map(map_name);
  }
});

function feature_added(event_triggered){
  //Check to see if there is a wkt field ready for this feature, if not, then create it
  var num_features = event_triggered.feature.layer.features.length;
  var last_feat_index = num_features-1;
  
  var field_name = event_triggered.feature.layer.map.map_name;
  var wkt_field = $("#edit-"+field_name+"-"+last_feat_index+"-wkt");
  
  if (wkt_field.size() == 0){
    $("#edit-"+field_name+"-"+field_name+"-add-more").trigger("mousedown");
  }
}
     
        function var_dump(element, limit, depth)
        {
          depth = depth?depth:0;
          limit = limit?limit:1;
          returnString = '<ol>';
          for(property in element)
          {
            //Property domConfig isn't accessable
            if (property != 'domConfig')
            {

              returnString += '<li><strong>'+ property + '</strong> <small>(' + (typeof element[property]) +')</small>';
              if (typeof element[property] == 'number' || typeof element[property] == 'boolean')
                returnString += ' : <em>' + element[property] + '</em>';
              if (typeof element[property] == 'string' && element[property])
                returnString += ': <div style="background:#C9C9C9;border:1px solid black; overflow:auto;"><code>' +
                          element[property].replace(/</g, '<').replace(/>/g, '>') + '</code></div>';
              if ((typeof element[property] == 'object') && (depth <limit))
                returnString += var_dump(element[property], limit, (depth + 1));
              returnString += '</li>';
            }
          }
          returnString += '</ol>';
          if(depth == 0)
          {
            winpop = window.open("", "","width=800,height=600,scrollbars,resizable");
            winpop.document.write('<pre>'+returnString+ '</pre>');
            winpop.document.close();
          }
          return returnString;
        }
      

function OnKeyDown(e){
      vKeyCode = e.keyCode;
      if ((vKeyCode == 63272)|| vKeyCode == 46){
        var vector_to_remove = maps[current_map]['layers']['vectors'].selectedFeatures[0];
        vector_to_remove.destroy();
        maps[current_map]['edit_controls'].modify.deactivate();
        maps[current_map]['edit_controls'].modify.activate();
      }
}

function draw_switch(field_name){
  if (!(maps[field_name]['edit_controls'].create.active)){
          maps[field_name]['edit_controls'].create.activate();
          maps[field_name]['edit_controls'].modify.activate();
          $("#draw_"+field_name).attr("src",geo_gui_module_path+"/draw_"+maps[field_name]['type']+"_on.png");
          $("#pan_"+field_name).attr("src",geo_gui_module_path+"/pan_off.png");
  }
}

function pan_switch(field_name){
  if (maps[field_name]['edit_controls'].create.active){
    maps[field_name]['edit_controls'].create.deactivate();
    maps[field_name]['edit_controls'].modify.deactivate();
    $("#draw_"+field_name).attr("src",geo_gui_module_path+"/draw_"+maps[field_name]['type']+"_off.png");
    $("#pan_"+field_name).attr("src",geo_gui_module_path+"/pan_on.png");
  }
}

//Switch between viewing it as an OpenLayers map and editing the WKT directly
function switch_map_wkt(field_name){
  if (maps[field_name]['active']){
    $("#"+field_name+"-items").css("display","block");
    $("#map_container_"+field_name).css("display","none");
    $("#switch_map_wkt_link_"+field_name).html("Edit using interactive map");
    map_to_wkt(field_name);
    maps[field_name]['active'] = false;
  }
  else{
    $("#"+field_name+"-items").css("display","none");
    $("#map_container_"+field_name).css("display","block");
    $("#switch_map_wkt_link_"+field_name).html("Edit using WKT");
    wkt_to_map(field_name);
    maps[field_name]['active'] = true;
  }
}

//Populate WKT values from vectors on the map
function map_to_wkt(field_name){
  //Clear existing values
  $("#"+field_name+"-items input[type=text]").val("");
  
  //Populate new values
  for (var i = 0; i < maps[field_name]['layers']['vectors'].features.length; i++) {
        var geometry = maps[field_name]['layers']['vectors'].features[i].geometry.clone();
        if (geo_to_ol_types[maps[field_name]['type']] == geometry.CLASS_NAME.substring(20)){ //Sometimes the wrong geometry gets drawn, so we impose a check
          geometry.transform(map_proj, maps[field_name]['dbproj']);
          var raw_wkt = geometry.toString();
          $("#edit-"+field_name+"-"+i+"-wkt").val(raw_wkt)
        }
   }
}

//Populate map vectors from WKT values
function wkt_to_map(field_name){
  //Clear existing vectors
  for (var i = maps[field_name]['layers']['vectors'].features.length; i != 0 ; i--) {
        maps[field_name]['layers']['vectors'].features[i-1].destroy();
  }
  maps[field_name]['layers']['vectors'].redraw();
  var features_to_add = [];
  
  //Add new vectors
  $("#"+field_name+"-items input[type=text]").each(function(){
    if ($(this).val() != ''){
      var wkt_ob = wkt.read($(this).val());
      if (typeof(wkt_ob) == "undefined"){
        alert("WKT is not valid");
      }
      else{
        wkt_ob.geometry.transform(maps[field_name]['dbproj'], map_proj);
        features_to_add.push(wkt_ob);
      }
    }
  });
  
  if (features_to_add.length != 0){
      maps[field_name]['layers']['vectors'].addFeatures(features_to_add);
  }
  
  if (!((maps[field_name]['type'] == 'point' && maps[field_name]['layers']['vectors'].features.length == 1) || maps[field_name]['layers']['vectors'].features.length == 0)){    //Don't find the bounds if there is only a single point. A bound on a single point is infinitely small
    var vector_bounds = get_bounds_of_layer(maps[field_name]['layers']['vectors']);
    maps[field_name]['map'].zoomToExtent(vector_bounds);
  }
}

//Get the bounds of a layer
function get_bounds_of_layer(input_layer){
  
  layer_bounds = new OpenLayers.Bounds();
  
  for (var i = 0; i < input_layer.features.length; i++) {
    layer_bounds.extend(input_layer.features[i].geometry.bounds);
  }
  
  return layer_bounds;
}